# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from .stock import (LocationDailyReportOpen, LocationDailyReportData,
    LocationDailyReport, LocationDailyReportRequired, StockConfiguration,
    LocationDailyReportExport, LocationDailyReportDataSearchTemplate,
    LocationDailyReportSaveTemplate)


def register():
    Pool.register(
        LocationDailyReportData,
        StockConfiguration,
        LocationDailyReportRequired,
        LocationDailyReportExport,
        LocationDailyReportDataSearchTemplate,
        LocationDailyReportSaveTemplate,
        module='stock_daily_report', type_='model')
    Pool.register(
        LocationDailyReportOpen,
        module='stock_daily_report', type_='wizard')
    Pool.register(
        LocationDailyReport,
        module='stock_daily_report', type_='report')
