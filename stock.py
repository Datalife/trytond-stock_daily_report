# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import io
import sys
import csv
import numbers
import tempfile
from itertools import groupby
from collections import OrderedDict
from trytond.model import ModelView, ModelSQL, Model, fields
from trytond.wizard import (Wizard, StateView, StateReport,
    StateTransition, Button)
from trytond.report import Report
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval, Bool, Not
from trytond.report.report import TranslateFactory
import datetime
from trytond.transaction import Transaction
import json


class LocationDailyReportDataSearchTemplate(ModelView, ModelSQL):
    """Location Daily Report Data Search Template"""
    __name__ = 'stock.location.daily_report.data.search.template'

    description = fields.Char('Description')
    raw_data_search = fields.Text('Raw data search')

    def get_rec_name(self, name):
        return self.description


class LocationDailyReportRequired(ModelView):
    """Stock Location Daily Report Required"""
    __name__ = 'stock.location.daily_report.required'

    location = fields.Many2One('stock.location', 'Location',
        required=True, domain=[
            ('type', 'in', ('storage', 'lost_found', 'drop',
                'customer', 'supplier'))])
    show_summary = fields.Boolean('Show summary')


class LocationDailyReportData(ModelView):
    """Stock Location Daily Report Data"""
    __name__ = 'stock.location.daily_report.data'

    from_date = fields.Date('From Date', required=True)
    to_date = fields.Date('To date', required=True)
    locations = fields.One2Many('stock.location.daily_report.required',
        None, 'Locations', required=True)
    all_categories = fields.Boolean('All product categories')
    categories = fields.Many2Many('product.category', None, None,
        'Product Categories', states={
            'readonly': Bool(Eval('all_categories')),
        }, depends=['all_categories'])
    products = fields.Many2Many('product.product', None, None, 'Products',
        states={
            'readonly': Bool(Eval('all_categories')) | Eval('categories'),
        }, depends=['all_categories', 'categories'])
    product_without_moves = fields.Boolean('Product without moves')
    report = fields.Many2One('ir.action.report',
        'Report', required=True, domain=[
            ('report_name', '=', 'stock.location.daily_report')
        ])
    search_template = fields.Many2One(
        'stock.location.daily_report.data.search.template',
        'Search template')

    def _load_template_instance(self, value):
        pool = Pool()
        if isinstance(value, dict):
            model = value.pop('model')
            Model = pool.get(model)
            return Model(**value)
        return value

    @fields.depends('search_template', 'locations', 'categories', 'products',
        '_parent_search_template.raw_data_search',
        methods=['_load_template_instance'])
    def on_change_search_template(self):
        if not self.search_template:
            self.locations = []
            self.categories = []
            self.products = []
            return
        data = json.loads(self.search_template.raw_data_search)

        for field_name, field_value in data.items():
            if isinstance(field_value, list):
                setattr(self, field_name, [])
                instances = [self._load_template_instance(instance)
                    for instance in field_value]
                setattr(self, field_name, instances)
            else:
                instance = self._load_template_instance(field_value)
                setattr(self, field_name, instance)

    @staticmethod
    def default_report():
        pool = Pool()
        Modeldata = pool.get('ir.model.data')

        return Modeldata.get_id('stock_daily_report', 'location_daily_report')

    @staticmethod
    def default_product_without_moves():
        return False

    @fields.depends('all_categories')
    def on_change_all_categories(self):
        if self.all_categories:
            self.categories = None
            self.products = None

    @fields.depends('categories')
    def on_change_categories(self):
        if self.categories:
            self.products = None

    @fields.depends('products')
    def on_change_products(self):
        if self.products:
            self.categories = None


class LocationDailyReportExport(ModelView):
    """Export Daily report"""
    __name__ = 'stock.location.daily_report.export'

    file = fields.Binary('File', readonly=True, filename='filename')
    filename = fields.Char('Filename', readonly=True)


class LocationDailyReportSaveTemplate(ModelView):
    """Location Daily Report Save Template"""
    __name__ = 'stock.location.daily_report.save_template'

    name = fields.Char('Name', required=True)
    template = fields.Many2One(
        'stock.location.daily_report.data.search.template', 'Template')


class LocationDailyReportOpen(Wizard):
    """Stock Location Daily Report Open"""
    __name__ = 'stock.location.daily_report.open'

    start = StateView('stock.location.daily_report.data',
        'stock_daily_report.location_daily_report_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Save template', 'save_template', 'tryton-save'),
            Button('Export', 'export', 'tryton-ok'),
            Button('Print', 'print_', 'tryton-print', default=True)
        ])
    print_ = StateReport('stock.location.daily_report')
    export = StateTransition()
    save_template = StateView('stock.location.daily_report.save_template',
        'stock_daily_report.location_daily_report_save_template_view_form', [
            Button('Go back', 'start', 'tryton-go-previous'),
            Button('Update current template',
                'update_template', 'tryton-save',
                states={
                    'invisible': Not(Bool(Eval('template', None)))
                }),
            Button('Create new template', 'new_template', 'tryton-new')
        ])
    update_template = StateTransition()
    new_template = StateTransition()
    result = StateView('stock.location.daily_report.export',
        'stock_daily_report.location_daily_report_export_view_form', [
            Button('Close', 'end', 'tryton-close')])

    def default_start(self, fields):
        pool = Pool()
        Date = pool.get('ir.date')
        default = {
            'from_date': Date.today(),
            'to_date': Date.today(),
        }
        return default

    def do_print_(self, action):
        pool = Pool()
        data = self._get_data()
        ActionReport = pool.get('ir.action.report')
        Action = pool.get('ir.action')

        action_id = self.start.report.id
        action_report = ActionReport(action_id)
        action = action_report.action
        action = Action.get_action_values(action.type, [action.id])[0]
        return action, data

    def _get_template_data(self):
        data = {}
        data['locations'] = [{
            'location': loc.location.id,
            'show_summary': loc.show_summary,
            'model': loc.__name__}
            for loc in self.start.locations]
        data['categories'] = [
            c.id for c in self.start.categories]
        data['all_categories'] = self.start.all_categories
        data['products'] = [
            c.id for c in self.start.products]
        data['product_without_moves'] = self.start.product_without_moves
        data['report'] = self.start.report.id
        return data

    def default_save_template(self, fields):
        if self.start.search_template:
            template = self.start.search_template
            return {'name': template.description,
                    'template': template.id}
        return {}

    def _save_template(self, template):
        data = self._get_template_data()
        template.raw_data_search = json.dumps(data)
        template.description = self.save_template.name
        template.save()

    def transition_update_template(self):
        self._save_template(self.start.search_template)
        return 'start'

    def transition_new_template(self):
        pool = Pool()
        Template = pool.get('stock.location.daily_report.data.search.template')
        template = Template()
        self._save_template(template)
        return 'start'

    def _get_data(self):
        show_locations = {l.location.id: l.show_summary
            for l in self.start.locations}
        return {
            'from_date': self.start.from_date,
            'to_date': self.start.to_date,
            'categories': [c.id for c in self.start.categories],
            'products': [c.id for c in self.start.products],
            'all_categories': self.start.all_categories,
            'show_locations': show_locations,
            'product_without_moves': self.start.product_without_moves,
            'ids': [location_show.location.id
                    for location_show in self.start.locations]
            }

    def transition_export(self):
        pool = Pool()
        Location = pool.get('stock.location')
        DailyReport = pool.get('stock.location.daily_report', type='report')

        data = self._get_data()
        ctx = DailyReport.get_context(Location.browse(data['ids']), None, data)

        file_name = self.generate_csv_report(ctx)
        self.result.file = io.open(file_name, 'r',
            encoding='utf-8').read().encode('utf-8')
        return 'result'

    def default_result(self, fields):
        file_ = self.result.file
        cast = self.result.__class__.file.cast
        self.result.file = False  # No need to store it in session
        return {
            'file': cast(file_) if file_ else None,
            'filename': 'stock.csv'
            }

    def generate_csv_report(self, ctx):
        pool = Pool()
        DailyReport = pool.get('stock.location.daily_report', type='report')
        Translation = pool.get('ir.translation')

        translate = TranslateFactory('stock.location.daily_report', Translation)

        decimal_point = Transaction().context['locale']['decimal_point']
        fileno, fname = tempfile.mkstemp('.csv', 'tryton_')
        if sys.version_info.major < 3:
            file_p = io.open(fname, 'wb+')
        else:
            file_p = io.open(fname, 'w+', encoding='utf-8')
        if decimal_point == ',':
            writer = csv.writer(file_p, delimiter=';')
        else:
            writer = csv.writer(file_p)

        def format_values(values):
            new_values = []
            for val in values:
                if isinstance(type(val), bytes):
                    new_values.append(val.replace('\n', ' ').replace(
                        '\t', ' '))
                elif isinstance(val, numbers.Number):
                    new_values.append(str(val))
                elif isinstance(val, str):
                    new_values.append(val)
                else:
                    new_values.append(val)
            return new_values

        writer.writerow(format_values(self._get_csv_report_header(ctx,
            translate)))

        for product, data in ctx['products_locations_moves'].items():
            for location, models in data.items():
                # initial stock
                init_row = self._get_csv_report_row(DailyReport, location,
                    product, None, model=translate('INITIAL STOCK'),
                    effective_date=self.start.from_date,
                    quantity=ctx['products_stock'].get(product.id, {}).get(
                        location.id, 0))
                writer.writerow(format_values(init_row))

                rows = []
                for model, moves in models.items():
                    if isinstance(model, str):
                        for _date, grouped_moves in groupby(moves[1:],
                                key=lambda x: x.effective_date):
                            grouped_moves = list(grouped_moves)
                            qty = sum(DailyReport.get_quantity(m, location)
                                for m in grouped_moves)
                            rows.append(
                                self._get_csv_report_row(DailyReport, location,
                                    product, None, model=translate(model),
                                    effective_date=_date,
                                    quantity=qty))
                        continue
                    _moves = moves[1:]
                    for move in _moves:
                        row = self._get_csv_report_row(DailyReport, location,
                            product, move, model=model.rec_name if
                            isinstance(model, Model) else model)
                        rows.append(row)

                # add rows ordered by date
                rows = sorted(rows, key=lambda x: (x[0], x[3]))
                for row in rows:
                    writer.writerow(format_values(row))

                # end stock
                writer.writerow(format_values(
                    self._get_csv_report_row(DailyReport, location, product,
                        None, model=translate('END STOCK'),
                        effective_date=self.start.to_date,
                        quantity=ctx['total_stock'][location.id][product.id])))
        file_p.close()
        return fname

    def _get_csv_report_header(self, ctx, translate):
        row = [
            translate('DATE'),
            translate('LOCATION'),
            translate('PRODUCT'),
            translate('CONCEPT'),
            translate('QUANTITY'),
        ]
        return row

    def _get_csv_report_row(self, ReportModel, location, product,
            move=None, **kwargs):
        assert product or move
        assert move or (set(['quantity', 'effective_date']) & set(
            kwargs.keys()) == set(['quantity', 'effective_date']))

        if product is None:
            product = move.product

        _concept = kwargs['model']
        if move is not None and (move.shipment or move.origin):
            _concept = '%s %s' % (_concept,
                (move.shipment or move.origin).rec_name)

        qty = ReportModel.get_quantity(move, location) if move else \
            kwargs.get('quantity', 0)
        res = [
            move.effective_date if move else kwargs['effective_date'],
            location.rec_name,
            product.name,
            _concept,
            qty,
        ]
        return res


class LocationDailyReport(Report):
    __name__ = 'stock.location.daily_report'

    @classmethod
    def get_context(cls, records, header, data):
        pool = Pool()
        Company = pool.get('company.company')

        report_context = super().get_context(records, header, data)
        context = Transaction().context
        report_context['company'] = Company(context['company'])

        products_locations_moves = OrderedDict()
        location_ids = []
        products_stock = {}
        total_stock = {}

        report_context['get_quantity'] = (lambda move,
            location: cls.get_quantity(move, location))
        report_context['total_consume'] = (lambda product, location,
            products_locations_moves: cls.get_total_consume(product, location,
                products_locations_moves))
        report_context['get_ordered_products'] = (lambda products:
            cls.get_ordered_products(products))
        product_filter = 'categories' if (
            data['all_categories'] or data['categories']) else 'id'
        filter_ids = data['categories']
        if product_filter == 'id':
            filter_ids = data['products']
        products = set()
        previous_day = data['from_date'] - datetime.timedelta(days=1)
        moves_by_location = []
        location_moves = {}

        for location in records:
            location_ids.append(location.id)
            moves_by_location = cls.get_moves_by_location(
                location, data['from_date'], data['to_date'],
                product_filter, filter_ids, data['all_categories'])
            for move in moves_by_location:
                products.add(move.product)
            if location not in location_moves:
                location_moves[location] = []
            location_moves[location].extend(moves_by_location)
        if data['product_without_moves']:
            products_no_moves, products_stock = cls.get_products_without_moves(
                product_filter, filter_ids, location_ids)
            products = products_no_moves.union(products)

        for location in records:
            total_stock[location.id] = {p.id: 0.0 for p in products}
            for move in location_moves[location]:
                products_locations_moves.setdefault(move.product, {})
                products_locations_moves[move.product].setdefault(location, {})
                _type = cls.get_move_type(move)
                products_locations_moves[move.product][
                    location].setdefault(_type, [0.0, ]).append(move)
                qty = cls.get_quantity(move, location)
                products_locations_moves[move.product][
                    location][_type][0] += qty

                total_stock[location.id][move.product.id] += qty

        for product in products:
            if product.id not in products_stock:
                products_stock[product.id] = cls.get_initial_stock_by_product(
                    total_stock, product, previous_day, location_ids)

        report_context['products_stock'] = products_stock
        report_context['products'] = products
        report_context['from_date'] = data['from_date']
        report_context['to_date'] = data['to_date']
        report_context['products_locations_moves'] = products_locations_moves
        report_context['total_stock'] = total_stock
        data['show_locations'] = {int(key): value
            for key, value in data['show_locations'].items()}
        return report_context

    @classmethod
    def get_initial_stock_by_product(cls, total_stock, product,
            previous_day, location_ids):
        Product = Pool().get('product.product')

        location_stock = {}
        stock_assign = ('assigned' in cls._get_moves_domain_states())
        with Transaction().set_context(stock_date_end=previous_day,
                    stock_assign=stock_assign):
            stock = Product.products_by_location(location_ids,
                with_childs=True, grouping=('product',),
                grouping_filter=([product.id], ))
            for key, quantity in stock.items():
                location_stock[key[0]] = quantity
                total_stock[key[0]][key[1]] += quantity
        return location_stock

    @classmethod
    def get_products_without_moves(cls, filter_field, filter_ids,
            location_ids):
        pool = Pool()
        Product = pool.get('product.product')

        location_stock = {}
        if filter_ids:
            products = Product.search([
                (filter_field, 'in', filter_ids)
            ])
        else:
            products = None
        stock = Product.products_by_location(location_ids,
                with_childs=True, grouping=('product',),
                grouping_filter=([p.id for p in products]
                    if products else None, ))

        products = set()
        for key, quantity in stock.items():
            location_stock[key[0]] = quantity
            products.add(Product(key[1]))
        return products, location_stock

    @classmethod
    def get_move_type(cls, move):
        if move.shipment:
            return cls.get_model_by_name(move.shipment.__name__)
        if move.origin:
            return cls.get_model_by_name(move.origin.__name__)
        loc_types = set((move.from_location.type, move.to_location.type))
        if 'production' in loc_types:
            return 'PRODUCTION'
        if 'lost_found' in loc_types or 'drop' in loc_types:
            return 'LOST'
        return None

    @classmethod
    def get_quantity(cls, move, location):
        return move.internal_quantity

    @classmethod
    def get_total_consume(cls, product, location, products_locations_moves):
        qty = 0.0
        if (location in products_locations_moves[product]):
            for item, moves in products_locations_moves[product][
                    location].items():
                qty += moves[0]
        return qty

    @classmethod
    def get_model_by_name(cls, name):
        Model = Pool().get('ir.model')

        res, = Model.search([('model', '=', name)])
        return res

    @classmethod
    def _get_moves_domain(cls, location, from_date, to_date,
            filter_type, filter_ids, all_categories):
        clauses = [
            ['OR',
                ('from_location', '=', location.id),
                ('to_location', '=', location.id),
            ],
            ('state', 'in', cls._get_moves_domain_states()),
            ('effective_date', '>=', from_date),
            ('effective_date', '<=', to_date),
            ('quantity', '!=', 0),
        ]
        filter_domain = [('product.%s' % filter_type, 'in', filter_ids)]
        if not all_categories:
            clauses.append(filter_domain)
        return clauses

    @classmethod
    def _get_moves_domain_states(cls):
        return ('assigned', 'done')

    @classmethod
    def get_moves_by_location(cls, location, from_date, to_date,
            filter_type, filter_ids, all_categories):
        Move = Pool().get('stock.move')
        clauses = cls._get_moves_domain(location, from_date, to_date,
            filter_type, filter_ids, all_categories)
        moves = Move.search(clauses, order=[
            ('product', 'ASC'),
            ('effective_date', 'ASC')]
        )
        for move in moves:
            if move.to_location.id != location.id:
                move.quantity *= -1
                move.internal_quantity *= -1
        return moves

    @classmethod
    def get_ordered_products(cls, products):
        return sorted(products, key=lambda p: p.name)


class StockConfiguration(metaclass=PoolMeta):
    __name__ = 'stock.configuration'

    @classmethod
    def __register__(cls, module_name):
        super(StockConfiguration, cls).__register__(module_name)
        table_h = cls.__table_handler__(module_name)

        # Migration: drop column daily_report_locations
        if table_h.column_exist('daily_report_locations'):
            table_h.drop_column('daily_report_locations')
