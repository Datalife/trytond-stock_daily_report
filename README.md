datalife_stock_daily_report
===========================

The stock_daily_report module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-stock_daily_report/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-stock_daily_report)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
