===========================
Stock Daily Report Scenario
===========================

Imports::

    >>> import datetime
    >>> import sys
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from proteus import Model, Wizard, Report
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> today = datetime.date(2018, 1, 2)

Install module::

    >>> config = activate_modules('stock_daily_report')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Reload the context::

    >>> User = Model.get('res.user')
    >>> config._context = User.get_preferences(True, config.context)

Get stock locations::

    >>> Location = Model.get('stock.location')
    >>> supplier_loc, = Location.find([('code', '=', 'SUP')])
    >>> storage_loc, = Location.find([('code', '=', 'STO')])
    >>> customer_loc, = Location.find([('code', '=', 'CUS')])
    >>> wh, = Location.find([('type', '=', 'warehouse')])

Create products::

    >>> ProductUom = Model.get('product.uom')
    >>> ProductTemplate = Model.get('product.template')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> template = ProductTemplate()
    >>> template.name = 'Product'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.list_price = Decimal('300')
    >>> template.cost_price = Decimal('80')
    >>> template.cost_price_method = 'average'
    >>> template.save()
    >>> product, = template.products

    >>> kg, = ProductUom.find([('name', '=', 'Kilogram')])
    >>> template2 = ProductTemplate()
    >>> template2.name = 'Product 2'
    >>> template2.default_uom = kg
    >>> template2.type = 'goods'
    >>> template2.list_price = Decimal('140')
    >>> template2.cost_price = Decimal('60')
    >>> template2.cost_price_method = 'average'
    >>> template2.save()
    >>> product2, = template2.products

Fill storage::

    >>> StockMove = Model.get('stock.move')
    >>> incoming_move = StockMove()
    >>> incoming_move.product = product
    >>> incoming_move.uom = unit
    >>> incoming_move.quantity = 1
    >>> incoming_move.from_location = supplier_loc
    >>> incoming_move.to_location = storage_loc
    >>> incoming_move.planned_date = today
    >>> incoming_move.effective_date = today
    >>> incoming_move.company = company
    >>> incoming_move.unit_price = Decimal('100')
    >>> incoming_move.currency = company.currency
    >>> incoming_moves = [incoming_move]

    >>> incoming_move = StockMove()
    >>> incoming_move.product = product2
    >>> incoming_move.uom = kg
    >>> incoming_move.quantity = 2.5
    >>> incoming_move.from_location = supplier_loc
    >>> incoming_move.to_location = storage_loc
    >>> incoming_move.planned_date = today
    >>> incoming_move.effective_date = today
    >>> incoming_move.company = company
    >>> incoming_move.unit_price = Decimal('70')
    >>> incoming_move.currency = company.currency
    >>> incoming_moves.append(incoming_move)
    >>> StockMove.click(incoming_moves, 'do')

Testing the report::

    >>> LocationShow = Model.get('stock.location.daily_report.required')
    >>> daily_report = Wizard('stock.location.daily_report.open')
    >>> daily_report.form.from_date = today
    >>> daily_report.form.to_date = today
    >>> ls = LocationShow(location=storage_loc, show_summary=True)
    >>> daily_report.form.all_categories = True
    >>> daily_report.form.locations.append(ls)
    >>> daily_report.execute('print_')

Testing the export::

    >>> daily_report = Wizard('stock.location.daily_report.open')
    >>> daily_report.form.from_date = today
    >>> daily_report.form.to_date = today
    >>> ls = LocationShow(location=storage_loc, show_summary=True)
    >>> daily_report.form.all_categories = True
    >>> daily_report.form.locations.append(ls)
    >>> daily_report.execute('export')
    >>> str(daily_report.form.file.decode('utf-8'))
    'DATE,LOCATION,PRODUCT,CONCEPT,QUANTITY\n2018-01-02,Storage Zone,Product,INITIAL STOCK,0\n2018-01-02,Storage Zone,Product,,1.0\n2018-01-02,Storage Zone,Product,END STOCK,1.0\n2018-01-02,Storage Zone,Product 2,INITIAL STOCK,0\n2018-01-02,Storage Zone,Product 2,,2.5\n2018-01-02,Storage Zone,Product 2,END STOCK,2.5\n'

Testing the templates::

    >>> Template = Model.get('stock.location.daily_report.data.search.template')
    >>> daily_report = Wizard('stock.location.daily_report.open')
    >>> ls = LocationShow(location=storage_loc, show_summary=True)
    >>> daily_report.form.all_categories = True
    >>> daily_report.form.locations.append(ls)
    >>> daily_report.execute('save_template')
    >>> daily_report.form.name = 'Search Template 1'
    >>> daily_report.execute('new_template')
    >>> template, = Template.find([])
    >>> template.description
    'Search Template 1'
    >>> len(daily_report.form.locations)
    0

After setting the template the fields are loaded::

    >>> daily_report.form.search_template = template
    >>> daily_report.form.all_categories
    True
    >>> len(daily_report.form.locations)
    1

Test update template::

    >>> daily_report.form.all_categories = False
    >>> daily_report.execute('save_template')
    >>> daily_report.execute('update_template')
    >>> daily_report.form.search_template = template
    >>> daily_report.form.all_categories
    False